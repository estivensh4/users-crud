import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { ROUTES } from './routes';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { AddUserComponent } from './add-user/add-user.component';

const routes: Routes = [
  { path: ROUTES.root, component: UsersListComponent },
  { path: ROUTES.addUser, component: AddUserComponent },
  { path: ROUTES.editUser + "/:id", component: UserDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
