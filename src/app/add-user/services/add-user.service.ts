import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AddUser } from '../interfaces/add-user';
import { UsersList } from 'src/app/users-list/interfaces/users-list';

@Injectable({
  providedIn: 'root'
})
export class AddUserService {

  constructor(private http: HttpClient) { }

  addUser(addUser: AddUser){
    return this.http.post<UsersList>("https://fakestoreapi.com/users", addUser)
  }
}
