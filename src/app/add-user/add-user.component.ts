import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddUserService } from './services/add-user.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpStatusCode } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ROUTES } from '../routes';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.sass']
})
export class AddUserComponent implements OnInit {
  constructor(
    private router: Router, 
    private addUserService: AddUserService,
     private fb: FormBuilder,
     private toastr: ToastrService,
     ){}
 
  addUserForm: FormGroup = this.fb.group({
    'email': ['', Validators.required],
    'username': ['', Validators.required],
    'phone': ['', Validators.required],
  });

  isLoading = false


  ngOnInit(): void {

  }


  addUser(){
    this.isLoading = true
    this.addUserService.addUser(this.addUserForm.value).subscribe(response => {
      this.isLoading = false
      this.toastr.success('Se ha agregado nuevo elemento correctamente', 'Estado')
      this.router.navigate([ROUTES.root])
    })
  }

  back(){
    this.router.navigate([ROUTES.root])
  }
}
