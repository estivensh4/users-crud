import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDetail } from '../interface/user-detail';

@Injectable({
  providedIn: 'root'
})
export class UserDetailService {

  constructor(private http: HttpClient) { }

  getUser(id: string | null): Observable<UserDetail>{
    return this.http.get<UserDetail>("https://fakestoreapi.com/users/" + id)
  }

  updateUser(form: UserDetail) {
    return this.http.put<UserDetail>("https://fakestoreapi.com/users/" + form.id, form)
  }
}
