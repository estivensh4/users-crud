import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDetailService } from './services/user-detail.service';
import { UserDetail } from './interface/user-detail';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ROUTES } from '../routes';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.sass']
})
export class UserDetailComponent implements OnInit {
  constructor(
    private activateRouter: ActivatedRoute,
    private router: Router,
    private userDetailService: UserDetailService,
    private fb: FormBuilder,
    private toastr: ToastrService
  ) { }

  user!: UserDetail;
  editUserForm: FormGroup = this.fb.group({
    id: ['', Validators.required],
    email: ['', Validators.required],
    username: ['', Validators.required],
    phone: ['', Validators.required]
  })

  isLoading = false

  ngOnInit(): void {
    let userId = this.activateRouter.snapshot.paramMap.get('id')
    this.userDetailService.getUser(userId).subscribe(data => {
      this.user = data
      this.editUserForm.setValue({
        'id': this.user.id!,
        'email': this.user.email,
        'phone': this.user.phone,
        'username': this.user.username,
      })
    })
  }

  updateUser() {
    this.isLoading = true
    this.userDetailService.updateUser(this.editUserForm.value).subscribe(response => {
      this.isLoading = false
      this.toastr.success('Se actualizado correctamente', 'Estado')
    })
  }

  back() {
    this.router.navigate([ROUTES.root])
  }



}
