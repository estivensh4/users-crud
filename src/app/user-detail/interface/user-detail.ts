export interface UserDetail {
    id: string,
    email: string,
    username: string,
    phone: string
}
