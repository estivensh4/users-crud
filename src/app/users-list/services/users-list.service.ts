import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersList } from '../interfaces/users-list';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersListService {

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<UsersList[]>{
    return this.http.get<UsersList[]>("https://fakestoreapi.com/users");
  }

  deleteUser(id: number) {

    return this.http.delete("https://fakestoreapi.com/users/" + id)
  }
}
