import { TestBed } from '@angular/core/testing';

import { UsersListService } from './users-list.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UsersListService', () => {
  let service: UsersListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(UsersListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
