export interface UsersList {
    id: number,
    email: string,
    phone: string,
    username: string
}
