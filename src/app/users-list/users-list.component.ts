import { Component, OnInit } from '@angular/core';
import { UsersListService } from './services/users-list.service';
import { UsersList } from './interfaces/users-list';
import { Router } from '@angular/router';
import { ROUTES } from '../routes';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.sass']
})
export class UsersListComponent implements OnInit {
  constructor(
    private usersListService: UsersListService,
    private router: Router,
    private toastr: ToastrService
  ) { }


  usersList: UsersList[] = [];
  isLoading = false


  ngOnInit(): void {
    this.isLoading = true
    this.usersListService.getAllUsers().subscribe(data => {
      this.isLoading = false
      this.usersList = data
    })
  }

  navigateToDetail(id: number) {
    this.router.navigate([ROUTES.editUser, id])
  }

  deleteUser(id: number) {
    this.isLoading = true
    this.usersListService.deleteUser(id).subscribe(response => {
      this.isLoading = false
      this.usersList = this.usersList.filter(item => item.id !== id)
      this.toastr.success('Se ha eliminado correctamente el registro ' + id, 'Estado')
    })
  }

  navigateAddUser() {
    this.router.navigate([ROUTES.addUser])
  }
}
